package fr.epsi.b3.inscription.service;

public class PersonneMineureException extends Exception {

	private static final long serialVersionUID = 6096373453329735370L;

	public PersonneMineureException(String message) {
		super(message);
	}

}
